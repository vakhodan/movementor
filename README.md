**MOVEMENT**OR
<p>
Mobilní aplikace s názvem MoveMentor poskytne uživatelům přístup k
různým tréninkovým programům, které mohou provádět doma, ve
fitness centrech nebo venku. Aplikace bude obsahovat rozsáhlou
knihovnu tréninků různých typů (např . kardio, síla, flexibility), které
budou vhodné pro různé úrovně fitness a cíle uživatelů.
</p>
